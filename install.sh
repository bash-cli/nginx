#!/bin/sh
./configure \
--sbin-path=/usr/local/bin \
--pid-path=/var/run/nginx.pid \
--conf-path=/etc/nginx/nginx.conf \
--http-log-path=/var/log/nginx/access.log \
--error-log-path=/var/log/nginx/error.log \
--http-client-body-temp-path=/var/tmp/nginx/body \
--http-fastcgi-temp-path=/var/tmp/nginx/fastcgi \
--http-proxy-temp-path=/var/tmp/nginx/proxy \
--http-scgi-temp-path=/var/tmp/nginx/scgi \
--http-uwsgi-temp-path=/var/tmp/nginx/uwsgi \
--with-pcre-jit \
--with-http_ssl_module \
--with-http_stub_status_module \
--with-http_realip_module \
--with-http_auth_request_module \
--with-http_addition_module \
--with-http_dav_module \
--with-http_gunzip_module \
--with-http_gzip_static_module \
--with-http_v2_module \
--with-threads \
--with-stream \
--with-stream_ssl_module


make && make install

mkdir -p /var/log/nginx/
mkdir -p /var/tmp/nginx/body
mkdir -p /var/tmp/nginx/fastcgi
mkdir -p /var/tmp/nginx/proxy
mkdir -p /var/tmp/nginx/scgi
mkdir -p /var/tmp/nginx/uwsgi
mkdir -p /etc/nginx/conf.d

rm -rf /usr/local/nginx && cp -r conf/ssl /etc/nginx/
chown -R www-data: /etc/nginx/ssl/letsencrypt
